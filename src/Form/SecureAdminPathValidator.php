<?php

namespace Drupal\secure_admin_path\Form;

use Drupal\secure_admin_path\EventSubscriber\SecureAdminPathEventSubscriber;

class SecureAdminPathValidator {

  /**
   * Force path replacement values to contain only lowercase letters, numbers,
   * and underscores.
   *
   * @param string $value
   *
   * @return boolean
   */
  public static function isValidPath(string $value): bool {
    return (bool) preg_match('~^[a-zA-Z0-9_-]+$~', $value);
  }

  /**
   * Verify users not overwriting with the default path names, could lead to
   * broken routes
   *
   * @param string $value
   *
   * @return bool
   */
  public static function isDefaultPath(string $value): bool {
    return in_array(
      strtolower($value),
      SecureAdminPathEventSubscriber::ADMIN_PATHS
    );
  }
}
